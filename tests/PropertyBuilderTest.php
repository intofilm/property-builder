<?php
/**
 * - PropertyBuilderTest.php
 *
 * @author  chris
 * @created 09/07/2018 09:49
 */

namespace IntoFilm\Sdk\Tests\Common\Property;

use IntoFilm\Sdk\Common\Property\PropertyBuilder;

class PropertyBuilderTest extends \PHPUnit_Framework_TestCase
{
    protected $schemaJson;

    /**
     * @param      $data
     * @param      $expected
     * @param null $namespace
     *
     * @dataProvider encoderProvider
     */
    public function testEncode($data, $expected, $namespace = null)
    {
        $builder = new PropertyBuilder(null, $namespace);
        $result  = $builder->encode($data);

        self::assertEquals($result, $expected);
        self::assertEquals($data, $builder->decode($result));
    }

    public function encoderProvider()
    {
        $in = [
            'jibber' => [
                'flibber' => 'flobber',
                'jabber'  => [
                    'foo',
                    'bar',
                    'baz',
                ],
            ],
            'great'  => true,
        ];

        return [
            // Standard
            [
                $in,
                [
                    'jibber.flibber'  => 'flobber',
                    'jibber.jabber.0' => 'foo',
                    'jibber.jabber.1' => 'bar',
                    'jibber.jabber.2' => 'baz',
                    'great'           => true,
                ],
            ],
            // With namespace
            [
                $in,
                [
                    'SOMEPREFIX.jibber.flibber'  => 'flobber',
                    'SOMEPREFIX.jibber.jabber.0' => 'foo',
                    'SOMEPREFIX.jibber.jabber.1' => 'bar',
                    'SOMEPREFIX.jibber.jabber.2' => 'baz',
                    'SOMEPREFIX.great'           => true,
                ],
                'SOMEPREFIX',
            ],
            // Empty values
            [
                [],
                [],
            ],
            // Empty values with namespace
            [
                [],
                [],
                'predator',
            ],
        ];
    }

    public function testWithSchema()
    {
        $builder = new PropertyBuilder(json_decode($this->schemaJson));
        $builder->encode(
            [
                "id"     => 1,
                "name"   => "John Doe",
                "orders" => [
                    [
                        "id" => 1,
                    ],
                    [
                        "id" => 4,
                        "price" => 1.0,
                    ],
                ],
            ]
        );
    }

    /**
     * @expectedException \Swaggest\JsonSchema\Exception\ObjectException
     * @expectedExceptionMessage Required property missing: id
     */
    public function testWithSchemaFail()
    {
        $builder = new PropertyBuilder(json_decode($this->schemaJson));
        $builder->encode(
            [
                "id"     => 1,
                "name"   => "John Doe",
                "orders" => [
                    [
                        "id" => 1,
                    ],
                    [
                        "price" => 1.0,
                    ],
                ],
            ]
        );
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Key path at fish of fish.0 cannot be indexed into (is not an array)
     */
    public function testEncodeClashingKeys()
    {
        $data = [
            'country'         => 'england',
            'fish'            => 'salmon',
            'fish.0'          => 'cod',
            'fish.1'          => 'bass',
            'fish.2'          => 'salmon',
            'fish.3'          => 'hadock',
            'fish.4'          => 'tuna',
            'GDPR-consent-id' => '68ff8966bbbd6089a4228f0428581d855a6a7b1b',
            'hello'           => 'foo',
            'weather'         => 'sun',
            'world'           => 'bar',
        ];

        $builder = new PropertyBuilder();
        $builder->decode($data);
    }

    protected function setUp()
    {
        $this->schemaJson = <<<'JSON'
{
    "type": "object",
    "properties": {
        "id": {
            "type": "integer"
        },
        "name": {
            "type": "string"
        },
        "orders": {
            "type": "array",
            "items": {
                "$ref": "#/definitions/order"
            }
        }
    },
    "required":["id"],
    "definitions": {
        "order": {
            "type": "object",
            "properties": {
                "id": {
                    "type": "integer"
                },
                "price": {
                    "type": "number"
                },
                "updated": {
                    "type": "string",
                    "format": "date-time"
                }
            },
            "required":["id"]
        }
    }
}
JSON;
    }

}

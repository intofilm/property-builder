<?php
/**
 * - PropertyBuilder.php
 *
 * @author  chris
 * @created 06/07/2018 16:48
 */

namespace IntoFilm\Sdk\Common\Property;

use Dflydev\DotAccessData\Data;
use Swaggest\JsonSchema\Schema;

class PropertyBuilder
{
    /**
     * @var Schema
     */
    protected $schema;

    /**
     * @var string
     */
    protected $namespace;

    /**
     * PropertyBuilder constructor.
     *
     * @param array  $schemaObject
     * @param string $namespace
     *
     * @throws \Swaggest\JsonSchema\Exception
     * @throws \Swaggest\JsonSchema\InvalidValue
     */
    public function __construct($schemaObject = null, $namespace = null)
    {
        $this->schema    = null === $schemaObject ? null : Schema::import($schemaObject);
        $this->namespace = $namespace;
    }

    /**
     * @param array $data
     *
     * @return array
     * @throws \Swaggest\JsonSchema\Exception
     * @throws \Swaggest\JsonSchema\InvalidValue
     */
    public function encode(array $data)
    {
        if (null !== $this->schema) {
            $this->schema->in(json_decode(json_encode($data)));
        }

        return self::flatten($data, $this->namespace) ?: [];
    }

    /**
     * @param $array
     *
     * @return array
     */
    public function decode(array $array)
    {
        $data = new Data();

        foreach ($array as $key => $value) {
            $data->set($key, $value);
        }

        $result = $data->export();

        if (null !== $this->namespace) {
            $result = isset($result[$this->namespace]) ? $result[$this->namespace] : [];
        }

        return $result;
    }

    /**
     * Flattens an array.
     *
     * @param array  $array
     * @param string $level
     *
     * @return array|bool
     */
    protected static function flatten($array, $level = null)
    {
        if (!is_array($array)) {
            return false;
        }

        $result = [];

        foreach ($array as $key => $value) {
            $flatKey = null === $level ? $key : implode('.', [$level, $key]);
            if (is_array($value)) {
                $result = array_merge($result, self::flatten($value, $flatKey));
            } else {
                $result[$flatKey] = $value;
            }
        }

        return $result;
    }
}
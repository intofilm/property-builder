# Using the Property builder

Encode and decode arrays of string to arrays of arrays and vice versa.

```php
<?php
$builder = new \IntoFilm\Sdk\Common\Property\PropertyBuilder(); 
$builder->encode(['foo' => ['bar', 'baz']]);
// [
//    'foo.0' => 'bar',
//    'foo.1' => 'baz'
// ]
```